package com.example.adrian.jumpcamera.ui.activities.BBDD;

/**
 * Created by oskit on 28/12/2017.
 */

public class Registro_Objeto {

    private String fecha;
    private float altura;
    private int id,id_u;

    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }


    public int getId_u() {
        return id_u;
    }

    public void setId_u(int id_u) {
        this.id_u = id_u;
    }



    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }



    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

}
