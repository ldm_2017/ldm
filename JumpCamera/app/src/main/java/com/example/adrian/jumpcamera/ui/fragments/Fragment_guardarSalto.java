package com.example.adrian.jumpcamera.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;

import org.joda.time.LocalDateTime;
import org.w3c.dom.Text;

import java.text.DecimalFormat;


public class Fragment_guardarSalto extends Fragment implements View.OnClickListener {

    // TODO: Rename and change types of parameters
    private int data;
    private double height;
    private double gravity;
    private  double vMax;
    private TextView timeinAir,heightText,speedText;
    private Button guardar, descartar;
    private String nombre_usu;


    public Fragment_guardarSalto() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View calcView = inflater.inflate(R.layout.fragment_fragment_guardar_salto, container, false);
        Bundle bundle = getArguments(); //cargarArgumentos

        instanciar(calcView);
        sacarUsuario();

        //Calculamos la altura
        DecimalFormat df = new DecimalFormat("#.00");
        DecimalFormat dfInt = new DecimalFormat("#");
        double timeAir = bundle.getDouble("data");
        //Calculo altura
        gravity = 9.81;
        double timeAirSeconds = (timeAir / 1000);     //Change the time to seconds and divide the time to get seconds
        double square = timeAirSeconds * timeAirSeconds; //We do square with Math.pow to obtain other part of the formula
        height = (gravity * square) / 8.0;
        height = height * 100; //Change to cm

        //Calculo VelocidadMaxima(TiempoDeDespegue)
        vMax = Math.sqrt((2*gravity)*(height/100)); //TODO: Arreglar la formula?


        timeinAir.setText(String.valueOf(dfInt.format(timeAir)));
        heightText.setText(String.valueOf(df.format(height)));
        speedText.setText(String.valueOf(df.format(vMax)));

        return calcView;
    }

    public void instanciar(View calcView){
        timeinAir = (TextView) calcView.findViewById(R.id.tiempo_vuelo);
        heightText = (TextView) calcView.findViewById(R.id.altura_salto);
        speedText = (TextView) calcView.findViewById(R.id.velocidadMax);

        guardar = (Button) calcView.findViewById(R.id.guardar_salto);
        descartar = (Button)calcView.findViewById(R.id.descartar_salto);

        guardar.setOnClickListener(this);
        descartar.setOnClickListener(this);


    }

    public void sacarUsuario(){

        SharedPreferences nom = getActivity().getSharedPreferences("Nombre_prefs", Context.MODE_PRIVATE);
        nombre_usu = nom.getString("nombre", "");


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    //CAPTURAR EL CLICK DE TODOS LOS BOTONES
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.guardar_salto:
                LocalDateTime localDateTime = new LocalDateTime();
                GestorBD gestorBD = new GestorBD(getContext());
                gestorBD.open();
                int id_u = gestorBD.getUsuarioID(nombre_usu);

                gestorBD.createRegistro(id_u,localDateTime.toString(),(float) height);

                Log.i("GUARDAR SALTO", localDateTime.toString());
                Log.i("GUARDAR SALTO", String.valueOf(height));

                gestorBD.close();

                Toast.makeText(getContext(), R.string.SaltoGuardado, Toast.LENGTH_SHORT).show();
                backToMainActinvity();

                break;

            case R.id.descartar_salto:

                Toast.makeText(getContext(), R.string.SaltoDescartado, Toast.LENGTH_SHORT).show();
                backToMainActinvity();

                break;
        }
    }

    public void backToMainActinvity() {

        Fragment fragmentGuardarSalto = new RecordVideoFragment();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, fragmentGuardarSalto).commit();

    }
}
