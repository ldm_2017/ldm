package com.example.adrian.jumpcamera.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;

import java.util.Arrays;

/**
 * Created by oskit on 04/01/2018.
 */

public class Login extends AppCompatActivity{

    private TextView registro, titulo;
    private EditText nom, pass;
    private Button entrar;
    private GestorBD gestorBD;

    private static final int SIGN_IN_CODE = 777; // Codigo X



    @Override
    protected void onDestroy() {
        super.onDestroy();
        gestorBD.close();
        Log.i("Ciclo de vida", "On Destroy");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);
        Log.i("Ciclo de vida", "On create");


        nom = (EditText) findViewById(R.id.new_usu);
        pass = (EditText) findViewById(R.id.contraseña);
        entrar = (Button) findViewById(R.id.entrar);
        registro = (TextView) findViewById(R.id.registro);


        setUser(); // Si viene de registro, dejar usuario relleno

        gestorBD = new GestorBD(this);
        gestorBD.open();


    }

    public void Ir_a_registro(View v) {
        Intent intent = new Intent(this, Registro.class);
        startActivity(intent);
    }

    public void Entrar(View view) {
        new Entrar().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    public class Entrar extends AsyncTask<Void, String, Void> {

        private String nombre;
        private String contraseña;
        private final String TAG_GUARDADO = "GUARDADO";
        private final String TAG_NO_EXISTE = "NO_EXISTE";
        private final String TAG_PASS_ERRONEA = "PASS_ERROR";
        private final String TAG_RELLENAR = "RELLENAR";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            nombre = nom.getText().toString().trim();
            contraseña = pass.getText().toString().trim();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            if ((nombre.equals("")) || contraseña.equals("")) {
                publishProgress(TAG_RELLENAR);

            } else if (!gestorBD.getUsuario(nombre).isEmpty()) {

                if (gestorBD.esUsuarioRegistrado(nombre, contraseña)) {
                    SharedPreferences prefs =
                            getSharedPreferences("Nombre_prefs", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("nombre", nombre);
                    editor.apply();
                    publishProgress(TAG_GUARDADO);

                } else
                    publishProgress(TAG_PASS_ERRONEA);

            } else
                publishProgress(TAG_NO_EXISTE);

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String msg = values[0];
            switch (msg) {
                case TAG_GUARDADO:
                    Intent main = new Intent(Login.this, MainActivity.class);
                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(main);
                    finish();
                    break;

                case TAG_PASS_ERRONEA:
                    Toast.makeText(getApplicationContext(), getString(R.string.Contraseña_erronea), Toast.LENGTH_SHORT).show();
                    break;
                case TAG_NO_EXISTE:
                    Toast.makeText(getApplicationContext(), getString(R.string.UsuNoExiste), Toast.LENGTH_SHORT).show();
                    break;
                case TAG_RELLENAR:
                    Toast.makeText(getApplicationContext(), getString(R.string.Rellenar_campos), Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //gestorBD.close();
            Log.i("Base datos", "Cerrado bd y dbconneccion");

        }

    }

    private void setUser() {
        Bundle b = getIntent().getExtras();
        if(b != null)
            nom.setText(b.getString("user"));
    }

}
