package com.example.adrian.jumpcamera.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;
import com.example.adrian.jumpcamera.ui.activities.BBDD.Usuario_Objeto;

import java.util.concurrent.TimeoutException;

import butterknife.Unbinder;

/**
 * Created by oskit on 02/01/2018.
 */

public class Fragment_usuarios extends Fragment {

    private String nombre_usu;
    private GestorBD gestorBD;
    private TextView nombre, contraseña, email;

    public static Fragment_usuarios newInstance(){
        Fragment_usuarios fragment = new Fragment_usuarios();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_usuarios, container, false);

        instanciar(view);

        //BBDD
        gestorBD.open();

        //Usuario
        sacarUsu();

        return view;


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gestorBD.close();
    }
    public void instanciar(View view){
        gestorBD = new GestorBD(getContext());
        nombre = (TextView)view.findViewById(R.id.nombre_usu);
        contraseña = (TextView)view.findViewById(R.id.contraseña_usu);
        email = (TextView)view.findViewById(R.id.email_usu2);

    }

    public void sacarUsu(){
        SharedPreferences nom = getActivity().getSharedPreferences("Nombre_prefs", Context.MODE_PRIVATE);
        nombre_usu = nom.getString("nombre", "");

        Usuario_Objeto usuarioObjeto = gestorBD.getUsuario(nombre_usu).get(0);

        nombre.setText(usuarioObjeto.getNombre());
        email.setText(usuarioObjeto.getEmail());
        contraseña.setText(usuarioObjeto.getContrasenia());

    }

}
