package com.example.adrian.jumpcamera.ui.activities.BBDD;

/**
 * Created by oskit on 28/12/2017.
 */

public class Usuario_Objeto {
    private int id;
    private String nombre;
    private String contrasenia;
    private String email;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }

}
