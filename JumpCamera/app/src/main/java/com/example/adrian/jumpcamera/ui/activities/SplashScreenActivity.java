package com.example.adrian.jumpcamera.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;
import com.example.adrian.jumpcamera.ui.activities.BBDD.Usuario_Objeto;

import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int SLEEP_TIME = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Thread thread = new Thread(new SleepWorker());
        thread.start();

    }

    private class SleepWorker implements Runnable{

        public void run() {

            try{
                Thread.sleep(SLEEP_TIME * 1000);

            }catch(InterruptedException e){
                e.printStackTrace();
            }
            Intent mainIntent;
            if (noUsuariosCreados()){
                mainIntent = new Intent(SplashScreenActivity.this, Login.class);

            }else {
                mainIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
            }
            startActivity(mainIntent);
            finish();


        }
    }

    public boolean noUsuariosCreados() {
        GestorBD gestorBD=new GestorBD(this);
        gestorBD.open();
        List<Usuario_Objeto> usuarios=gestorBD.getAllUsuarios();
        gestorBD.close();
        return usuarios.isEmpty();
    }


}
