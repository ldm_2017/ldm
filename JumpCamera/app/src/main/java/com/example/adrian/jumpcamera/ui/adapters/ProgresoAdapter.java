package com.example.adrian.jumpcamera.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adrian.jumpcamera.Model.tupla_progreso;
import com.example.adrian.jumpcamera.R;

import java.util.ArrayList;

/**
 * Created by oskit on 03/01/2018.
 */
public class ProgresoAdapter
        extends RecyclerView.Adapter<ProgresoAdapter.ProgresoViewHolder>
        implements View.OnClickListener{

    private View.OnClickListener listener;
    private View.OnLongClickListener listener2;
    private ArrayList<tupla_progreso> datos;



    public static class ProgresoViewHolder
            extends RecyclerView.ViewHolder {

        private TextView fecha,altura;
        private ImageView flecha;

        public ProgresoViewHolder(View itemView) {
            super(itemView);

            fecha = (TextView)itemView.findViewById(R.id.fecha_tv);
            altura = (TextView)itemView.findViewById(R.id.altura_progreso);
            flecha=(ImageView)itemView.findViewById(R.id.image_flecha);

        }

        public void bindTitular(tupla_progreso t) {
            fecha.setText(t.getFecha());
            altura.setText(String.valueOf(t.getAltura())+"cm");
            flecha.setImageResource(t.getIcono());

        }
    }

    public ProgresoAdapter(ArrayList<tupla_progreso> datos) {
        this.datos = datos;
    }

    @Override
    public ProgresoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.tupla_progreso, viewGroup, false);



        ProgresoViewHolder tvh = new ProgresoViewHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(ProgresoViewHolder viewHolder, int pos) {
        tupla_progreso item = datos.get(pos);

        viewHolder.bindTitular(item);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public tupla_progreso getItem(int position){
        return datos.get(position);
    }


    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }
    //  public void setOnLongClickListener(View.OnLongClickListener listener2){this.listener2=listener2;}


    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }
}
