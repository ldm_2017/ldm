package com.example.adrian.jumpcamera.ui.fragments;

import android.content.Intent;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.adrian.jumpcamera.BuildConfig;
import com.example.adrian.jumpcamera.R;

import net.protyposis.android.mediaplayer.MediaPlayer;
import net.protyposis.android.mediaplayer.MediaSource;
import net.protyposis.android.mediaplayer.VideoView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class VideoFragment extends Fragment {


    private Unbinder unbinder;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    private File mVideoFolder;
    private String mVideoFilename;
    private String path;
    Timer timer;
    ImageButton btnPlay, btnPause, btnRewind, btnForward;
    Button btnBegin, btnEnd;
    int beginPos;
    int endPos;
    boolean enable = false;
    double timeAir;

    public VideoFragment() {
        // Required empty public constructor
    }

    public static VideoFragment newInstance() {

        VideoFragment fragment = new VideoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View calcView = inflater.inflate(R.layout.fragment_video, container, false);

        unbinder = ButterKnife.bind(this.getContext(), calcView);
        btnPlay = (ImageButton) calcView.findViewById(R.id.btnplay);
        btnPause = (ImageButton) calcView.findViewById(R.id.btnpause);
        btnRewind = (ImageButton) calcView.findViewById(R.id.btnrewind);
        btnForward = (ImageButton) calcView.findViewById(R.id.btnforward);
        btnBegin = (Button) calcView.findViewById(R.id.btnbegin);
        btnEnd = (Button) calcView.findViewById(R.id.btnend);

        try {
            createVideoFolder();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return calcView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void createVideoFolder() throws IOException {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        mVideoFolder = new File(movieFile, "JumpCamera");
        if (!mVideoFolder.exists()) {
            mVideoFolder.mkdirs();
            createVideoFileName();
        }
        createVideoFileName();


    }

    File createVideoFileName() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "Prueba_" + timestamp;
        File videoFile = File.createTempFile(prepend, ".mp4", mVideoFolder);
        mVideoFilename = videoFile.getAbsolutePath();
        videoFile.delete();
        startRecord();
        return videoFile;
    }


    public void startRecord() {
        int requestCode = 0;
        int resultCode = 1;

        String timestamp = new SimpleDateFormat("yyyy_MM_dd_HHmmss").format(new Date());
        String prepend = "Salto_" + timestamp + ".mp4";
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "/JumpCamera", prepend);
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        Uri outputFileUri = FileProvider.getUriForFile(this.getContext(), BuildConfig.APPLICATION_ID + ".provider", file);
        path = file.getAbsolutePath();

        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);

            onActivityResult(requestCode, resultCode, takeVideoIntent);


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Toast toast = Toast.makeText(this.getContext(), "video guardado en:" + path, Toast.LENGTH_LONG);
            toast.show();
            editVideoFramebyFrame(path);
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_CANCELED) {
            Toast.makeText(this.getContext(), this.getContext().getString(R.string.cancel), Toast.LENGTH_SHORT).show();


        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void editVideoFramebyFrame(final String path) {

        final long timeTotal;
        final Long timeFrame;


        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        MediaExtractor extractor = new MediaExtractor();

        int frameRate = 0; //may be default
        try {
            //Adjust data source as per the requirement if file, URI, etc.
            extractor.setDataSource(path);
            int numTracks = extractor.getTrackCount();
            for (int i = 0; i < numTracks; ++i) {
                MediaFormat format = extractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);
                if (mime.startsWith("video/")) {
                    if (format.containsKey(MediaFormat.KEY_FRAME_RATE)) {
                        frameRate = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //Release stuff
            extractor.release();
        }

        retriever.setDataSource(path);

        timeTotal = Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        long numberFrames = (timeTotal / 1000) * frameRate;
        timeFrame = timeTotal / numberFrames;

        final VideoView videoView = (VideoView) getView().findViewById(R.id.videoView);
        videoView.requestFocus();
        videoView.setVideoPath(path);


        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final MediaPlayer mp) {
                videoView.setSeekMode(MediaPlayer.SeekMode.EXACT);
                videoView.seekTo(1000);
                mp.setVolume(0f, 0f);

                videoView.pause();


            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                    if (videoView.isPlaying()) {
                        videoView.pause();
                    } else {
                        videoView.start();
                    }

            }
        });

        btnBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginPos = videoView.getCurrentPosition();
                btnSaveEnabled(enable, beginPos, endPos);

            }
        });
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endPos = videoView.getCurrentPosition();
                btnSaveEnabled(enable, beginPos, endPos);

                if (beginPos != 0 && endPos != 0) {
                    timeInAir(endPos, beginPos);
                }
                backToMainActinvity();

            }
        });


        btnRewind.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (videoView == null) {
                    return;
                }

                int pos = videoView.getCurrentPosition();
                if (pos > 0) {
                    pos -= timeFrame;
                    videoView.seekTo(pos);
                    videoView.pause();
                } else if (pos == 0 || pos < 0) {
                    Toast.makeText(getContext(), getString(R.string.videoBegin), Toast.LENGTH_SHORT).show();

                } else if (pos < 0) {
                    Toast.makeText(getContext(), getString(R.string.videoError), Toast.LENGTH_SHORT).show();
                }


            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (videoView == null) {
                    return;
                }
                int pos = videoView.getCurrentPosition();
                if (pos < timeTotal && pos > 0) {
                    pos += timeFrame;
                    videoView.seekTo(pos);
                    videoView.pause();
                } else if (pos == timeTotal || pos > timeTotal) {
                    Toast.makeText(getContext(), getString(R.string.videoEnd), Toast.LENGTH_SHORT).show();


                } else if (pos < 0) {
                    Toast.makeText(getContext(), getString(R.string.videoError), Toast.LENGTH_SHORT).show();
                }

            }

        });


    }

    public double timeInAir(int endPos, int beginPos) {
        timeAir = 0;
        timeAir = endPos - beginPos;

        Toast.makeText(getContext(), "TimeInAir: " + timeAir, Toast.LENGTH_SHORT).show();



        return timeAir;
    }


    public void btnSaveEnabled(boolean enable, int beginPos, int endPos) {

        if (beginPos != 0 && endPos != 0 ) {
            enable = true;

        } else if (endPos == 0) {
            Toast.makeText(getContext(), getString(R.string.endTime), Toast.LENGTH_SHORT).show();

        } else if (beginPos == 0) {
            Toast.makeText(getContext(), getString(R.string.beginTime), Toast.LENGTH_SHORT).show();

        } else if ( beginPos==endPos) {

            Toast.makeText(getContext(), getString(R.string.timeError), Toast.LENGTH_SHORT).show();
        }
        else if ( beginPos>endPos) {

            Toast.makeText(getContext(), getString(R.string.changeTime), Toast.LENGTH_SHORT).show();
        }
    }

    public void backToMainActinvity() {

        Fragment fragmentGuardarSalto = new Fragment_guardarSalto();

        Bundle  bundle = new Bundle(); //guardarArgumentos
        bundle.putDouble("data",timeAir);//guardarArgumentos
        fragmentGuardarSalto.setArguments(bundle);//guardarArgumentos

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, fragmentGuardarSalto).commit();

    }
}



