package com.example.adrian.jumpcamera.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;

/**
 * Created by oskit on 04/01/2018.
 */

public class Registro extends AppCompatActivity{

    private EditText nom,pass,rep_pass, email;

    private GestorBD gestorBD=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Log.i("Ciclo de vida","On create");

       instanciar();

        // DDBB
        gestorBD=new GestorBD(this);
        gestorBD.open();

    }
    public void instanciar(){
        nom=(EditText)findViewById(R.id.new_usu_reg);
        pass=(EditText)findViewById(R.id.contraseña_reg);
        rep_pass=(EditText)findViewById(R.id.rep_contraseña);
        email = (EditText) findViewById(R.id.email_reg);


    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Ciclo de vida","On Pause");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        gestorBD.close();
        Log.i("Ciclo de vida","On Destroy y cerrar DB");
    }

    public void Crear(View view){
        new Crear().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public class Crear extends AsyncTask<Void, String ,Void> {
        private String nombre;
        private String contraseña;
        private String rep_contraseña;
        private String e_mail;
        private final String TAG_GUARDADO = "GUARDADO";
        private final String TAG_PASS = "PASS_DIFF";
        private final String TAG_RELLENAR = "RELLENAR";
        private final String TAG_EXISTE = "YA_EXISTE";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            nombre = nom.getText().toString().trim();
            contraseña = pass.getText().toString().trim();
            rep_contraseña = rep_pass.getText().toString().trim();
            e_mail = email.getText().toString().trim();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            if ((nombre.equals("")) || contraseña.equals("") || e_mail.equals("")) {
                publishProgress(TAG_RELLENAR);
            } else if (!contraseña.equals(rep_contraseña)) {
                publishProgress(TAG_PASS);
            } else if (!gestorBD.getUsuario(nombre).isEmpty()) {
                publishProgress(TAG_EXISTE);
            } else {
                SharedPreferences prefs = getSharedPreferences("Nombre_prefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("nombre", nombre);
                editor.apply();

                gestorBD.createUsuario(nombre,contraseña, e_mail);
                publishProgress(TAG_GUARDADO);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String msg = values[0];

            switch (msg) {
                case TAG_RELLENAR:
                    Toast.makeText(getApplicationContext(), getString(R.string.Rellenar_campos), Toast.LENGTH_SHORT).show();
                    break;
                case TAG_PASS:
                    Toast.makeText(getApplicationContext(), getText(R.string.Contraseña_diff), Toast.LENGTH_SHORT).show();
                    break;
                case TAG_EXISTE:
                    Toast.makeText(getApplicationContext(), getText(R.string.UsuYaExiste), Toast.LENGTH_SHORT).show();
                    break;
                case TAG_GUARDADO:
                    Toast.makeText(getApplicationContext(), getText(R.string.Us_guardado), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(Registro.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    Bundle b = new Bundle();
                    b.putString("user", nom.getText().toString());

                    intent.putExtras(b);

                    startActivity(intent);
                    break;
            }
        }
    }
}
