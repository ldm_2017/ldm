package com.example.adrian.jumpcamera.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adrian.jumpcamera.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Adrian on 01/01/2018.
 */

public class LibraryVideoFragment extends Fragment {

    private static final String TAG ="" ;
    private Unbinder unbinder;
    private Context context;
    private String s;

    public void LibraryVideoFragment(Context context){
        context= this.context;

    }
    public static LibraryVideoFragment  newInstance() {
        LibraryVideoFragment fragment = new LibraryVideoFragment();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View calcView = inflater.inflate(R.layout.fragment_library, container, false );
        unbinder = ButterKnife.bind(this, calcView);
        return calcView;

    }

}
