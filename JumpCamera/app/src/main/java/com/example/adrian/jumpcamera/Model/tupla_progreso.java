package com.example.adrian.jumpcamera.Model;

/**
 * Created by oskit on 03/01/2018.
 */

public class tupla_progreso {

    private String fecha;
    private float altura;
    private int icono;

    public tupla_progreso(String fecha, float altura, int icono) {
        this.fecha = fecha;
        this.altura = altura;
        this.icono = icono;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }
}
