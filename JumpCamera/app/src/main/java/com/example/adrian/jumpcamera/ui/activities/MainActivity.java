package com.example.adrian.jumpcamera.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.fragments.Fragment_progreso;
import com.example.adrian.jumpcamera.ui.fragments.Fragment_usuarios;
import com.example.adrian.jumpcamera.ui.fragments.RecordVideoFragment;



public class MainActivity extends AppCompatActivity{

   BottomNavigationView bottomNavigationView;
   RecordVideoFragment recordVideoFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        instanciar();
        acciones();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            verifyPermission();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void verifyPermission() {
        int permsRequestCode = 0;
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        int accessWritePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = checkSelfPermission(Manifest.permission.CAMERA);

        if (cameraPermission == PackageManager.PERMISSION_GRANTED &&  accessWritePermission == PackageManager.PERMISSION_GRANTED) {
            permsRequestCode = 1;
            //se realiza metodo si es necesario...
        } else {
            permsRequestCode = 2;
            requestPermissions(perms, permsRequestCode);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                break;

        case 2:

            Toast toast = Toast.makeText(this,getString(R.string.denied),Toast.LENGTH_LONG);
            toast.show();

            break;
        }
    }

    public void instanciar(){
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

    }
    public void acciones(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                Activity selected = null;
                switch (item.getItemId()){

                    case R.id.historial_tab:
                        selectedFragment = Fragment_progreso.newInstance();
                        break;
                    case R.id.salto_tab:
                        selectedFragment = RecordVideoFragment.newInstance();
                        break;
                    case R.id.ususario_tab:
                        selectedFragment = Fragment_usuarios.newInstance();
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
                return true;
            }
        });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, RecordVideoFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);

    }



}
