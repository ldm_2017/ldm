package com.example.adrian.jumpcamera.ui.activities.BBDD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oskit on 28/12/2017.
 */

public class GestorBD {

    private SQLiteDatabase database=null;
    private DBhelper helper=null;

    private String[] allColumnsUsuario={DBhelper.USUARIO_ID, DBhelper.USUARIO_NOMBRE, DBhelper.CONTRASENIA, DBhelper.EMAIL};
    private String[] allColumnsRegistro={DBhelper.ID_REG, DBhelper.ID_U, DBhelper.FECHA, DBhelper.ALTURA};


    public GestorBD(Context context){
        helper=new DBhelper(context);
    }



    public void open() throws SQLException {

        if (database == null )
            database = helper.getWritableDatabase();

    }
    //Cerrar base de datos

    public void close(){
        if (database != null)
            database.close();
    }

    /**--------------------------CONSULTAS PARA TABLA REGISTRO--------------------**/

    public Registro_Objeto createRegistro(int id_u, String fecha, float altura){
        ContentValues values=new ContentValues();
        values.put(DBhelper.ID_U, id_u);
        values.put(DBhelper.FECHA, fecha);
        values.put(DBhelper.ALTURA, altura);

        long insertId = database.insert(DBhelper.REGISTRO_TABLA, null,
                values);
        Cursor cursor = database.query(DBhelper.REGISTRO_TABLA,
                allColumnsRegistro, DBhelper.ID_REG + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Registro_Objeto newRegistro = cursorToRegistro(cursor);
        cursor.close();
        return newRegistro;
    }

    public void deleteRegistro(Registro_Objeto registro) {
        long id = registro.getId();
        System.out.println("Registro borrado con id: " + id);
        database.delete(DBhelper.REGISTRO_TABLA, DBhelper.ID_REG
                + " = " + id, null);
    }

    public List<Registro_Objeto> getAllRegistros(int id_u) {
        List<Registro_Objeto> registros = new ArrayList<>();
        String[] args= new String[]{String.valueOf(id_u)};

        Cursor cursor = database.query(DBhelper.REGISTRO_TABLA,
                allColumnsRegistro, DBhelper.ID_U+"=?", args, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Registro_Objeto registro = cursorToRegistro(cursor);
            registros.add(registro);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return registros;
    }

    public List<Registro_Objeto> getRegistroFecha(int id_u, String fecha) {
        List<Registro_Objeto> registros = new ArrayList<>();
        String[] args= new String[]{String.valueOf(id_u),fecha};

        Cursor cursor = database.query(DBhelper.REGISTRO_TABLA,
                allColumnsRegistro, DBhelper.ID_U+"=? and "+DBhelper.FECHA+"=?", args, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Registro_Objeto registro = cursorToRegistro(cursor);
            registros.add(registro);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return registros;
    }

    public Registro_Objeto getRegistroByID(int id){

        String [] args= {String.valueOf(id)};

        Cursor cursor= database.query(DBhelper.REGISTRO_TABLA, allColumnsRegistro, DBhelper.ID_REG+"=?", args, null , null, null);

        Registro_Objeto registro=null;
        if(cursor.moveToFirst())
            registro=cursorToRegistro(cursor);
        cursor.close();

        return registro;
    }

    public float getRecord(int id_u){

        String[] ids={String.valueOf(id_u)};
        String[] altura_max={"MAX("+DBhelper.ALTURA+")"};

        Cursor cursor=database.query(DBhelper.REGISTRO_TABLA, altura_max,
                DBhelper.ID_U+"=?",ids, null, null, null);

        float record=0;
        if(cursor.moveToFirst()) {

            record = cursor.getFloat(0);
        }
        cursor.close();

        return record;
    }

    //Si en una misma fecha hay varios RM, saca el registro con el mayor de ellos
    public List<Registro_Objeto> getRecordGroupByFecha(int id_u){
        List<Registro_Objeto> registros = new ArrayList<>();
        String[] args={String.valueOf(id_u)};

        Cursor cursor = database.query(DBhelper.REGISTRO_TABLA,
                allColumnsRegistro, DBhelper.ID_U+"=?", args, DBhelper.FECHA, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Registro_Objeto registro = cursorToRegistro(cursor);
            registros.add(registro);
            cursor.moveToNext();
        }
        cursor.close();
        return registros;

    }

    private Registro_Objeto cursorToRegistro(Cursor cursor) {
        Registro_Objeto registro = new Registro_Objeto();
        registro.setId(cursor.getInt(0));
        registro.setId_u(cursor.getInt(1));
        registro.setFecha(cursor.getString(2));
        registro.setAltura(cursor.getFloat(3));
        return registro;
    }




    /**------------------CONSULTAS PARA TABLA USUARIOS-----------------------------**/

    public Usuario_Objeto createUsuario(String nombre, String contraseña, String email){
        ContentValues values=new ContentValues();
        values.put(DBhelper.USUARIO_NOMBRE, nombre);
        values.put(DBhelper.CONTRASENIA, contraseña);
        values.put(DBhelper.EMAIL, email);
        long insertId = database.insert(DBhelper.USUARIOS_TABLA, null,
                values);
        Cursor cursor = database.query(DBhelper.USUARIOS_TABLA,
                allColumnsUsuario, DBhelper.USUARIO_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Usuario_Objeto newUsuario = cursorToUsuario(cursor);
        cursor.close();
        return newUsuario;
    }

    public void deleteUsuario(Usuario_Objeto usuario) {
        long id = usuario.getId();
        System.out.println("Usuario borrado con id: " + id);
        database.delete(DBhelper.USUARIOS_TABLA, DBhelper.USUARIO_ID
                + " = " + id, null);
    }

    public List<Usuario_Objeto> getAllUsuarios() {
        List<Usuario_Objeto> usuarios = new ArrayList<>();

        Cursor cursor = database.query(DBhelper.USUARIOS_TABLA,
                allColumnsUsuario, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Usuario_Objeto usuario = cursorToUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return usuarios;
    }

    public List<Usuario_Objeto> getUsuario(String nombre){

        List<Usuario_Objeto> usuarios = new ArrayList<>();

        Cursor cursor= database.query(DBhelper.USUARIOS_TABLA, allColumnsUsuario,
                DBhelper.USUARIO_NOMBRE+"=?", new String[]{nombre}, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Usuario_Objeto usuario = cursorToUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return usuarios;
    }
    public int getUsuarioID(String nombre){



        Cursor cursor= database.query(DBhelper.USUARIOS_TABLA, allColumnsUsuario,
                DBhelper.USUARIO_NOMBRE+"=?", new String[]{nombre}, null, null, null);

        Usuario_Objeto usuario =null;
        if(cursor.moveToFirst()) {

            usuario=cursorToUsuario(cursor);
            return usuario.getId();
        }

        // make sure to close the cursor
        cursor.close();
        return 0;

    }


    public boolean esUsuarioRegistrado(String nombre, String contraseña){

        List<Usuario_Objeto> usuarios = new ArrayList<>();

        Cursor cursor= database.query(DBhelper.USUARIOS_TABLA, allColumnsUsuario,
                DBhelper.USUARIO_NOMBRE+"=? and "+DBhelper.CONTRASENIA+"=?", new String[]{nombre,contraseña}, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Usuario_Objeto usuario = cursorToUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return !usuarios.isEmpty();

    }

    private Usuario_Objeto cursorToUsuario(Cursor cursor) {
        Usuario_Objeto usuario = new Usuario_Objeto();
        usuario.setId(cursor.getInt(0));
        usuario.setNombre(cursor.getString(1));
        usuario.setContrasenia(cursor.getString(2));
        usuario.setEmail(cursor.getString(3));
        return usuario;
    }


    /**-----------------------------------BASE DE DATOS---------------------------**/
    private class DBhelper extends SQLiteOpenHelper{

        private SQLiteDatabase db;

        // Información de la tabla usuario
        private static final String USUARIOS_TABLA = "Usuarios";

        private static final String USUARIO_ID = "_id";
        private static final String USUARIO_NOMBRE = "Nombre";
        private static final String CONTRASENIA = "Contraseña";
        private static final String EMAIL = "Email";

        //Informacion de la tabla Registro_Entrenamiento
        private static final String REGISTRO_TABLA = "Registro";

        private static final String ID_REG = "_id";
        private static final String ID_U = "u_id";
        private static final String FECHA = "Fecha";
        private static final String ALTURA = "Altura";

        //Informacion de la BBDD
        private static final String DB_NAME = "JumpCameraDB";
        //Subir uno cada vez que se actualize la BD
        private static final int DB_VERSION = 1;

        //Creacion Tabla USUARIOS
        private static final String CREATE_TABLE1 = "create table "
                + USUARIOS_TABLA + "(" + USUARIO_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USUARIO_NOMBRE + " TEXT unique NOT NULL,"
                + CONTRASENIA + " TEXT,"
                + EMAIL + " TEXT" + ");";
        //Creación Tabla Registro
        private static final String CREATE_TABLE2 = "create table "
                + REGISTRO_TABLA + "(" + ID_REG
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ID_U + " INTEGER,"
                + FECHA + " TEXT,"
                + ALTURA + " REAL,"
                + "FOREIGN KEY ( " + ID_U + ") REFERENCES " + USUARIOS_TABLA + " (" + USUARIO_ID + ") ON DELETE CASCADE);";


        public DBhelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
            db = getReadableDatabase();
        }


        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("PRAGMA foreign_keys=1;");
            sqLiteDatabase.execSQL(CREATE_TABLE1);
            sqLiteDatabase.execSQL(CREATE_TABLE2);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        }
    }
}
