package com.example.adrian.jumpcamera.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.adrian.jumpcamera.Model.DividerItemDecorator;
import com.example.adrian.jumpcamera.Model.tupla_progreso;
import com.example.adrian.jumpcamera.R;
import com.example.adrian.jumpcamera.ui.activities.BBDD.GestorBD;
import com.example.adrian.jumpcamera.ui.activities.BBDD.Registro_Objeto;
import com.example.adrian.jumpcamera.ui.adapters.ProgresoAdapter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by oskit on 02/01/2018.
 */

public class Fragment_progreso extends Fragment {
    private RecyclerView recyclerView;
    private ListView listView;
    private BarChart grafica;
    private GestorBD gestorBD;
    private LinearLayoutManager linearLayout;
    private ArrayList<tupla_progreso> items_lista;
    private ProgresoAdapter adaptador;

    private String nombre_usu;
    private String[] listaFech;   //Lista con las fechas no repetidas

    public static Fragment_progreso newInstance() {
        Fragment_progreso fragment = new Fragment_progreso();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_progreso, container, false);
        //Vistas
        instanciar(view);
        //Usuario
        sacarUsu();
        //BBDD
        gestorBD.open();
        //Lista
        recyclerView.setHasFixedSize(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(adaptador);
        recyclerView.addItemDecoration(new DividerItemDecorator(getContext(), DividerItemDecorator.VERTICAL_LIST));

        // Gráfico
        grafica.setDescription("");
        grafica.setFitBars(true);
        grafica.getAxisRight().setDrawLabels(false);
        grafica.setDrawGridBackground(false);
        grafica.setDrawBorders(false);
        grafica.setDoubleTapToZoomEnabled(false);
        grafica.setVisibleXRangeMaximum(7);       // MAX. 7 registros si .setTouchEnabled == false
        grafica.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

            }

            @Override
            public void onChartLongPressed(MotionEvent me) {
                grafica.highlightValues(null);

            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {

            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {

            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {

            }
        });
        new ObtieneSaltos().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return view;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gestorBD.close();
    }


    public void instanciar(View view) {
        gestorBD = new GestorBD(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_progreso);
        grafica = (BarChart) view.findViewById(R.id.graph_prog);
        linearLayout = new LinearLayoutManager(getContext());
        items_lista = new ArrayList<tupla_progreso>();
        adaptador = new ProgresoAdapter(items_lista);
    }

    public void sacarUsu() {
        SharedPreferences nom = getActivity().getSharedPreferences("Nombre_prefs", Context.MODE_PRIVATE);
        nombre_usu = nom.getString("nombre", "");
    }

    private class ObtieneSaltos extends AsyncTask<Void, String, List<Registro_Objeto>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            items_lista.clear();
            adaptador.notifyDataSetChanged();
        }

        @Override
        protected List<Registro_Objeto> doInBackground(Void... voids) {

            List<Registro_Objeto> allRegister = new ArrayList<Registro_Objeto>();


            //Saco el usuario
            int id_u = gestorBD.getUsuarioID(nombre_usu);

            //Consulta de todas las fechas por usuario
            allRegister = gestorBD.getAllRegistros(id_u);

            //Ordenar la lista por fecha (primer elemento el más moderno, último el más viejo)
            if (!allRegister.isEmpty()) {
                Collections.sort(allRegister, new Comparator<Registro_Objeto>() {
                    @Override
                    public int compare(Registro_Objeto o1, Registro_Objeto o2) {
                        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
                        DateTime date1 = dtf.parseDateTime(o1.getFecha());
                        DateTime date2 = dtf.parseDateTime(o2.getFecha());

                        return date2.compareTo(date1);
                    }
                });

                String fecha;
                float altura;

                //Creamos las tuplas y le ponemos el icono correspondiente
                for (int i = 0; i < allRegister.size(); i++) {
                    fecha = allRegister.get(i).getFecha();
                    altura = allRegister.get(i).getAltura();
                    //Cambiamos el formato de la fecha para mostrarla
                    //Formato de origen
                    DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
                    //Parseo de String a tipo DateTime
                    DateTime fechaDate = dtf.parseDateTime(fecha);
                    //Formato que queremos
                    DateTimeFormatter dtfOut = DateTimeFormat.forPattern("EEEEEEEEE, d MMM yyyy");
                    fecha = dtfOut.print(fechaDate);

                    //Este es el último elemento, o lo que es lo mismo, el primer registro de entreno
                    if (i == allRegister.size() - 1) {
                        items_lista.add(new tupla_progreso(fecha, altura, R.drawable.novariation_icon));

                    } else {

                        if (allRegister.get(i).getAltura() < allRegister.get(i + 1).getAltura()) {
                            items_lista.add(new tupla_progreso(fecha, altura, R.drawable.downarrow_icon));

                        } else if (allRegister.get(i).getAltura() > allRegister.get(i + 1).getAltura()) {
                            items_lista.add(new tupla_progreso(fecha, altura, R.drawable.up_icon));
                        } else
                            items_lista.add(new tupla_progreso(fecha, altura, R.drawable.novariation_icon));
                    }
                }
                publishProgress("actualizar", "actualizar");


            } else {
                publishProgress("vacio", "vacio");
            }

            return allRegister;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            String msg = values[0];

            if (msg.equals("actualizar")) {
                adaptador.notifyDataSetChanged();
            }
        }

        @Override
        protected void onPostExecute(List<Registro_Objeto> lista_ordenada) {
            super.onPostExecute(lista_ordenada);

            if (!lista_ordenada.isEmpty()) {
                new DibujarRecta().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, lista_ordenada);
            } else {
                BarData data = new BarData();
                grafica.setData(data);
                grafica.invalidate();
            }

        }
    }

    private class DibujarRecta extends AsyncTask<List<Registro_Objeto>, Void, List<IBarDataSet>> {

        @Override
        protected List<IBarDataSet> doInBackground(List<Registro_Objeto>... lista_delistas) {
            //Como es una lista de listas, sacamos el primero que es la lista que nos interesa
            int cntArray = 0;
            List<Registro_Objeto> lista_ordenada = new ArrayList<Registro_Objeto>();
            lista_ordenada = lista_delistas[0];
            int numero_de_registros = lista_ordenada.size();
            listaFech = new String[lista_ordenada.size()];
            String[] listaFechComp = new String[lista_ordenada.size()];

            List<BarEntry> progreso = new ArrayList<BarEntry>();
            DateTimeFormatter originalFormat2 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

            for (Registro_Objeto registro : lista_ordenada) {

                float rm = registro.getAltura();
                numero_de_registros = numero_de_registros - 1;

                DateTime fechaConTodo = originalFormat2.parseDateTime(registro.getFecha());
                String fechaReducida = fechaConTodo.toString(DateTimeFormat.shortDate());
                listaFechComp[cntArray] = fechaReducida;
                cntArray++;

                BarEntry e1 = new BarEntry(numero_de_registros, rm);
                progreso.add(e1);
            }

            for (int i = 0; i < listaFechComp.length; i++) {
                listaFech[listaFechComp.length - i - 1] = listaFechComp[i];
            }

            BarDataSet set1 = new BarDataSet(progreso, "Progreso");
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            List<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            return dataSets;
        }

        @Override
        protected void onPostExecute(List<IBarDataSet> iBarDataS) {
            super.onPostExecute(iBarDataS);

            XAxis xAxis = grafica.getXAxis();
            xAxis.setValueFormatter(new HourAxisValueFormatter(listaFech));
            xAxis.setGranularity(1f);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextSize(10f);
            xAxis.setTextColor(R.color.colorPrimary);
            xAxis.setDrawAxisLine(true);
            xAxis.setDrawGridLines(false);
//            xAxis.setAxisMaxValue(100);
//            xAxis.setAxisMinValue(0);

            BarData data = new BarData(iBarDataS);
            data.setBarWidth(0.9f);
            grafica.setData(data);
            grafica.animateY(1700);

            if (listaFech.length != 0) {
                MyMarkerView myMarkerView = new MyMarkerView(getContext(), R.layout.detalle_grafica_progreso, listaFech);
                grafica.setMarkerView(myMarkerView);
            } else {
                Log.e("Markerview", grafica.getMarkerView().toString());
            }
            grafica.invalidate();

        }
    }

    private class HourAxisValueFormatter implements AxisValueFormatter {
        private String[] listaFech;

        public HourAxisValueFormatter(String[] listaFech) {
            this.listaFech = listaFech;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //Cuando hay 1 solo valor, value==-1, con lo cual hay que diferenciar ese caso
            if ((int) value < listaFech.length) {
                if (listaFech.length == 1) {
                    return (listaFech[0]);
                } else {
                    return (listaFech[(int) value]);
                }
            } else {
                return "";
            }
        }

        public int getDecimalDigits() {
            return 0;
        }
    }

    private class MyMarkerView extends MarkerView {

        private TextView tvContent;
        private String[] listaFech;  // minimum timestamp in your data set
        private float xOffsetMultiplier;

        public MyMarkerView(Context context, int layoutResource, String[] listaFech) {
            super(context, layoutResource);
            // this markerview only displays a textview
            tvContent = (TextView) findViewById(R.id.tvContent);
            this.listaFech = listaFech;
            //  this.mDataFormat = new SimpleDateFormat("dd-MM-yy", Locale.ENGLISH);
            //  this.mDate = new Date();
        }

        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            // long currentTimestamp = (int)e.getX() + referenceTimestamp;

            if (listaFech.length >= 6) {
                if (e.getX() < 2) {
                    xOffsetMultiplier = 5.1f;
                    tvContent.setBackgroundResource(R.drawable.marker2_left);
                    tvContent.setText(e.getY() + "cm");//getTimedate(currentTimestamp)); // set the entry-value as the display text //
                    //Cambiar maximaLongitud para saber el ultimo dato de la grafica
                } else if (e.getX() > listaFech.length - 3) {
                    xOffsetMultiplier = 1.20f;
                    tvContent.setBackgroundResource(R.drawable.marker2_right);
                } else {
                    xOffsetMultiplier = 2f;
                    tvContent.setBackgroundResource(R.drawable.marker2);
                }
            } else {
                xOffsetMultiplier = 2f;
            }
            tvContent.setText(e.getY() + "cm at " + listaFech[(int) e.getX()]);//getTimedate(currentTimestamp)); // set the entry-value as the display text //

        }

        @Override
        public int getXOffset(float xpos) {
            return (int) -(getWidth() / xOffsetMultiplier);
        }

        @Override
        public int getYOffset(float ypos) {
            return -getHeight();
        }

    }

}



