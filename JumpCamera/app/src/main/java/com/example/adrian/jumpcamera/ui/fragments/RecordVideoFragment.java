package com.example.adrian.jumpcamera.ui.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.adrian.jumpcamera.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Adrian on 01/01/2018.
 */

public class RecordVideoFragment extends Fragment {

    private Unbinder unbinder;


    public RecordVideoFragment() {
    }

    public static RecordVideoFragment newInstance() {
        RecordVideoFragment fragment = new RecordVideoFragment();
        return fragment;

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View calcView = inflater.inflate(R.layout.fragment_record, container, false);
        unbinder = ButterKnife.bind(this.getContext(), calcView);
        return calcView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton floatingActionButton = (FloatingActionButton) getView().findViewById(R.id.recordVideoButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                            verifyPermission();
                                                        }

                                                    }

                                                }
        );

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void verifyPermission() {
        int permsRequestCode = 0;
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        int accessWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);

        if (cameraPermission == PackageManager.PERMISSION_GRANTED && accessWritePermission == PackageManager.PERMISSION_GRANTED) {
            permsRequestCode = 1;
            newFragment();
        } else {
            permsRequestCode = 2;
            getActivity().requestPermissions(perms, permsRequestCode);

        }
        //requestPermissions(perms,permsRequestCode);
        getActivity().requestPermissions(perms, permsRequestCode);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0:
                newFragment();
                break;
            case 1:
                newFragment();
                break;

            case 2:
                Toast toast = Toast.makeText(this.getContext(), getString(R.string.denied), Toast.LENGTH_LONG);
                toast.show();
                break;
        }
    }

    public void newFragment() {

        Fragment videoFragment = new VideoFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, videoFragment).addToBackStack(null).commit();

    }

}



